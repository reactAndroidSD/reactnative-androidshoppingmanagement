import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView } from 'react-native';

export default class HelloWorldApp extends Component {
  
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2});
  }
  
  render() {
    return (
      <View>
        <View style={{backgroundColor:"rgb(215, 0, 0)",height:70}}>
          <Text style={{fontSize:30,marginBottom:10,padding:3,shadowRadius:0.5,color:"white"}}>Sản phẩm</Text>
        </View>
        <View>
          <ListView
          dataSource={
            this.ds.cloneWithRows([
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
              {tensanpham:"áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff",
               masanpham:"123456_Vangcuc-XL-CT-6-12 Tuổi-onau-M-TT-Slim fit-Button cuf",
               giavon:"500.000",
               giaban:"600.000	",
               ton:"2000",
               cotheban:"1500"
              },
            ])
          }
          renderRow = {(rowData)=>{
            return(
                // <Text> Hello </Text>
                <View style = {a.bao}> 
          <View style = {a.trai}><Text>hinh anh</Text>
          </View>
          <View style = {a.phai}>
          <Text>{rowData.tensanpham}</Text> 
          <Text style={{color:"grey"}}>{rowData.masanpham}</Text>
              <View style={{flexDirection:"row"}}>
                <View style={{flex:1}}>
                  <Text>Giá vốn:0</Text>
                  <Text>Giá bán:0</Text>
                </View>
                <View style={{flex:1}}>
                  <Text style={{textAlign:"right"}}>Tồn:0</Text>
                  <Text style={{textAlign:"right"}}>Có thể bán:0</Text>
                </View>
              </View>
          </View>
        </View>
            )
            
          }}
        />
        </View>
      
         {/* <View style = {a.bao}> 
          <View style = {a.trai}><Text>hinh anh</Text>
          </View>
          <View style = {a.phai}>
          <Text>Ten san pham</Text> 
          <Text style={{color:"grey"}}>ma san pham</Text>
              <View style={{flexDirection:"row"}}>
                <View style={{flex:1}}>
                  <Text>Giá vốn:0</Text>
                  <Text>Giá bán:0</Text>
                </View>
                <View style={{flex:1}}>
                  <Text style={{textAlign:"right"}}>Tồn:0</Text>
                  <Text style={{textAlign:"right"}}>Có thể bán:0</Text>
                </View>
              </View>
          </View>
        </View>
        <View style = {a.bao}> 
          <View style = {a.trai}><Text>hinh anh</Text>
          </View>
          <View style = {a.phai}>
          <Text>Ten san pham</Text> 
          <Text style={{color:"grey"}}>ma san pham</Text>
              <View style={{flexDirection:"row"}}>
                <View style={{flex:1}}>
                  <Text>Giá vốn:0</Text>
                  <Text>Giá bán:0</Text>
                </View>
                <View style={{flex:1}}>
                  <Text style={{textAlign:"right"}}>Tồn:0</Text>
                  <Text style={{textAlign:"right"}}>Có thể bán:0</Text>
                </View>
              </View>
          </View>
        </View>  */}
      </View>
        
         
    );
  }
}
 var a = StyleSheet.create({
   bao:{
    flexDirection:"row",
    borderBottomWidth: 1,
    marginBottom:2,
    padding:4
   },
   trai:{
    flex:1,
    flexDirection:"row",
   },
   phai:{
    flex:3
   },
 });