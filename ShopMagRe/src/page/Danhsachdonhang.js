import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView } from 'react-native';

export default class HelloWorldApp extends Component {
  
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2});
  }
  
  render() {
    return (
      <View>
        <View style={{backgroundColor:"rgb(215, 0, 0)",height:70}}>
          <Text style={{fontSize:30,marginBottom:10,padding:3,shadowRadius:0.5,color:"white"}}>Đơn hàng</Text>
        </View>
        <View>
          <ListView
          dataSource={
            this.ds.cloneWithRows([
              {
               ID:"#1231241",
               tenkhachhang:"Văn",
               diachikhachhang:"Hồ Chí Minh, quận 1",
               sodienthoaikhachhang:"0120491744",
               thanhtoan:"615.000"
              },
              {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
               {
                ID:"#1231241",
                tenkhachhang:"Văn",
                diachikhachhang:"Hồ Chí Minh, quận 1",
                sodienthoaikhachhang:"0120491744",
                thanhtoan:"615.000"
               },
            ])
          }
          renderRow = {(rowData)=>{
            return(
                <View style = {a.bao}> 
                  <View style = {a.trai}><Text>check-box</Text>
                   </View>
                    <View style = {a.phai}>
                      <Text>{rowData.ID}</Text> 
                      <Text style={{color:"grey"}}>{rowData.tenkhachhang}</Text>
                <View style={{flexDirection:"row"}}>
                  <View style={{flex:3}}>
                    <Text>{rowData.diachikhachhang}</Text>
                    <Text>{rowData.sodienthoaikhachhang}</Text>
                  </View>
                  <View style={{flex:1}}>
                    <Text style={{textAlign:"right"}}>{rowData.thanhtoan}</Text>
                  </View>
              </View>
          </View>
        </View>
            )
            
          }}
        />
        </View>
      
         {/* <View style = {a.bao}> 
          <View style = {a.trai}><Text>hinh anh</Text>
          </View>
          <View style = {a.phai}>
          <Text>Ten san pham</Text> 
          <Text style={{color:"grey"}}>ma san pham</Text>
              <View style={{flexDirection:"row"}}>
                <View style={{flex:1}}>
                  <Text>Giá vốn:0</Text>
                  <Text>Giá bán:0</Text>
                </View>
                <View style={{flex:1}}>
                  <Text style={{textAlign:"right"}}>Tồn:0</Text>
                  <Text style={{textAlign:"right"}}>Có thể bán:0</Text>
                </View>
              </View>
          </View>
        </View>
        <View style = {a.bao}> 
          <View style = {a.trai}><Text>hinh anh</Text>
          </View>
          <View style = {a.phai}>
          <Text>Ten san pham</Text> 
          <Text style={{color:"grey"}}>ma san pham</Text>
              <View style={{flexDirection:"row"}}>
                <View style={{flex:1}}>
                  <Text>Giá vốn:0</Text>
                  <Text>Giá bán:0</Text>
                </View>
                <View style={{flex:1}}>
                  <Text style={{textAlign:"right"}}>Tồn:0</Text>
                  <Text style={{textAlign:"right"}}>Có thể bán:0</Text>
                </View>
              </View>
          </View>
        </View>  */}
      </View>
        
         
    );
  }
}
 var a = StyleSheet.create({
   bao:{
    flexDirection:"row",
    borderBottomWidth: 1,
    marginBottom:2,
    padding:4
   },
   trai:{
    flex:1,
    flexDirection:"row",
   },
   phai:{
    flex:5
   },
 });