import React, { Component } from 'react';
import { Text, View, StyleSheet, ListView, Image, ScrollView } from 'react-native';

export default class HelloWorldApp extends Component {
  
  constructor(props) {
    super(props);

    this.ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!==r2});
  }
  
  render() {
    return (
      <ScrollView>
      <View>
        <View style={{backgroundColor:"rgb(215, 0, 0)",height:70}}>
          <Text style={{fontSize:30,marginBottom:10,padding:3,shadowRadius:0.5,color:"white"}}>Chi tiết sản phẩm</Text>
        </View>
        
        <View>
          <ListView
          dataSource={
            this.ds.cloneWithRows([
              {},
            ])
          }
          renderRow = {(rowData)=>{
            return(
              <View style={{flexDirection:"column"}}>
              {/* Show số tồn */}
              <View style={{flexDirection:"column"}}>
                <View style={{flexDirection:"row"}}>
                  <View style={a.amount}>
                    <Text>TỒN</Text>
                    <Text>7</Text>
                  </View>
                  <View style={a.amount}>
                  <Text>CÓ THỂ BÁN</Text>
                  <Text>7</Text>
                  </View>
                  <View style={a.amount}>
                  <Text>ĐANG GIAO</Text>
                  <Text>7</Text>
                  </View>
                </View>
                <View style={{flexDirection:"row"}}>
                <View style={a.amount}>
                    <Text>LỖI</Text>
                    <Text>7</Text>
                  </View>
                  <View style={a.amount}>
                  <Text>CHUYỂN KHO</Text>
                  <Text>7</Text>
                  </View>
                  <View style={a.amount}>
                  <Text>TẠM GIỮ</Text>
                  <Text>7</Text>
                  </View>
                </View>
              </View>
      
      
              {/* Show hình */}
              <View style={{backgroundColor:"white",flexDirection:"row"}}>
                <Image source = {{uri:'https://pbs.twimg.com/profile_images/486929358120964097/gNLINY67_400x400.png'}}
                    style = {{ width: 100, height: 100 }}
                />
              </View>
      
              {/* Show thông tin sản phẩm */}
              <View style={{flexDirection:"column"}}>
                <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Tên sản phẩm</Text>
                <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Mã sản phẩm</Text>
                   <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Danh mục</Text>
                   <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Sản phẩm cha</Text>
                   <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Giá bán lẻ</Text>
                   <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                    <Text style={a.nameAttitude}>Giá bán buôn</Text>
                    <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                    <Text style={a.nameAttitude}>Giá nhập</Text>
                     <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
              </View>

                {/* Show thuộc tính còn lại của sản phẩm */}
                <View style={{backgroundColor:"white",flexDirection:"row",paddingTop:50,paddingLeft:10,paddingBottom:10}}>
                  <Text style={{fontSize:30}}>Thuộc tính sản phẩm</Text>
                </View>
                <View style={{flexDirection:"column"}}>
                <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Màu sắc</Text>
                <Text> Tím</Text>
                </View>
                <View style={a.viewAttitude}>
                <Text style={a.nameAttitude}>Kích thước</Text>
                <Text>XL</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Mã sản phẩm</Text>
                   <Text>áo tay dài - Vàng cúc - XL - Cotton - 6-12 Tuổi - Ô nâu - Nam - Trang phục thời trang - Slim fit - Button cuff</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Nhà sản xuất</Text>
                   <Text>Unifarm</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Mô tả</Text>
                   <Text> Không có mô tả</Text>
                </View>
                <View style={a.viewAttitude}>
                   <Text style={a.nameAttitude}>Bảo hành</Text>
                   <Text>Bảo hành trong vòng 1 năm</Text>
                </View>
            
              </View>

            </View> 
             )
            
          }}
        />
        </View>
       
      </View>
      </ScrollView>
        
         
    );
  }
}
 var a = StyleSheet.create({
   bao:{
    flexDirection:"row",
    borderBottomWidth: 1,
    marginBottom:2,
    padding:4
   },
   trai:{
    flex:1,
    flexDirection:"row",
   },
   phai:{
    flex:3
   },
   amount:{
    flex:1,
    borderWidth:1,
    borderColor:"gray",
    padding:15
   },
   nameAttitude:{
   color:"gray"
   },
   viewAttitude:{
    borderBottomWidth:1,
    padding:10,
    marginTop:5
   }
 });