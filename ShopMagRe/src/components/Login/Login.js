//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import LoginForm from './LoginForm';

// create a component
class Login extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>      
                    <Image
                    style ={styles.logo}
                    source={require('../images/Shopping.png')} 
                    />

                    <Text style={styles.title}>Ứng dụng quản lý bán hàng</Text>

                                 
                </View>

                <View style={styles.formContainer}>
                    <LoginForm />
                </View>

            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3498db'
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center',
    },
    logo: {
        width: 100,
        height: 100,
    },
    title: {
        color: '#FFF',
        textAlign: 'center',
        marginTop: 10,
        width: 200,
        opacity: 1.2
    }

});

//make this component available to the app
export default Login;
