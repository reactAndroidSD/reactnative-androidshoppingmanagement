import { Navigation } from 'react-native-navigation';

import HomeScreen from './screens/HomeScreen';
import AnalyticsScreen from './screens/AnalyticsScreen';
import SideDrawer from './screens/SideDrawer';
import React, {Component} from 'react';

Navigation.registerComponent('drawer.HomeScreen', () => HomeScreen);
Navigation.registerComponent('drawer.SideDrawer', () => SideDrawer);
Navigation.registerComponent('drawer.AnalyticsScreen', () => AnalyticsScreen);



// Navigation.startSingleScreenApp(
//   {
//     screen:{
//       screen: 'drawer.HomeScreen',
//       title: 'Home'
//     }
//   }
// )

Navigation.startSingleScreenApp({
  screen: {
    screen: 'drawer.HomeScreen',
      title: 'Home'
  }
});

// Navigation.startSingleScreenApp({
// 	screen: {
// 		screen: 'app.TopTabsScreen',
// 		title: 'Interview',
// 		navigatorStyle,
// 		topTabs:[
//               {
//                   screenId: 'app.Interview',
//                   title: 'Technical',
//       				},
// 							{
//                   screenId: 'app.Interview',
//                   title: 'HR',
//       				},
//               {
//                   screenId: 'app.Interview',
//                   title: 'Aptitude',
//       				},
// 						]
// 					},
// 	drawer: {
// 		left: {
// 			screen: 'app.Drawer'
// 		}
// 	}
// });





// import React, {Component} from 'react';
// import {Platform, StyleSheet, Text, FlatList, View, Button, TextInput, TouchableOpacity } from 'react-native';
// import { createStackNavigator } from 'react-navigation';
// import newPage from './src/newPage'




// export default class App extends React.Component {

// //   navigateToNew = () => {
// //     navigation.navigate('newPage');
// // }
// render() {
//   const { navigate } = this.props.navigation;

 
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>Welcome to React Native!</Text>
//         <Button
//             title="New Page here"
//             onPress={ navigate('RootStack') }
//             //this.navigateToNew
//          />
//       </View>


//     );
  
// }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });


// const RootStack = createStackNavigator({
//   Home: {
//     screen: App,
//     newscreen: newPage,
//   },
// });