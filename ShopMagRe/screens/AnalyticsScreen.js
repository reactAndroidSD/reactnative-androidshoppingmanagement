// import React, { Component } from 'react';
// import { View, Text } from 'react-native';

// export default class AnalyticsScreen extends Component {

//   constructor(props) {
//     super(props);
//     this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
//   }

//   onNavigatorEvent = event => {
//     if(event.type === "NavBarButtonPress") {
//       if(event.id === "sideDrawerToggle") {
//         this.props.navigator.toggleDrawer({
//           side: 'left'     
//         });
//       }
//     }
//   }

//   render() {
//     return (
//       <View>
//         <Text> Your AnalyticsScreen </Text>
//       </View>
//     )
//   }
// }

import React, { Component } from 'react';
import { View, Text,Button } from 'react-native';
import startMainTab from './startMainTab';

export default class HomeScreen extends Component {

  onButtonPress = () => {
    startMainTab();
  }

  render() {
    return (
      <View>
        <Text>Home Screen</Text>
        <Button title="Tab Navigation" onPress = { this.onButtonPress } />
      </View>
    );
  }
}