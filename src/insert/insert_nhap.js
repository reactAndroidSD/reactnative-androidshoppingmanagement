import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_nhap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            soluong: '',
            id_nsx: '',
            ghichu: '',
        }
    }

    componentDidMount1() {
        fetch('http://192.168.1.215:3000/themnhasanxuat', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                soluong: parseInt(this.state.soluong),
                id_nsx: parseInt(this.state.id_nsx),
                ghichu: this.state.ghichu,
            }),
        });
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>

                    <ScrollView>
                        <View>
                            <Text>Số lượng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />

                            <Text>Mã nhà sản xuất: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(id_nsx) => this.setState({ id_nsx })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />
                            
                            <Text>Ghi chú: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}


                            />

                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}
