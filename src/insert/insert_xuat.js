import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_xuat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ghichu: '',
            id_sp: 0,
            soluong: 0,
            tenkh: '',
            sdt: '',
            diachi: '',
        }
    }

    componentDidMount1() {
        fetch('http://192.168.1.215:3000/themdonxuatkho', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({

                ghichu: this.state.ghichu,
                id_sp: parseInt(this.state.id_sp),
                soluong: parseInt(this.state.soluong),
                tenkh: this.state.tenkh,
                sdt: this.state.sdt,
                diachi: this.state.diachi,
            }),
        });
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>

                    <ScrollView>
                        <View>
                            <Text>Sản phẩm: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_sp) => this.setState({ id_sp })}


                            />

                            <Text>Số lượng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(soluong) => this.setState({ soluong })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Tên Khách hàng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(tenkh) => this.setState({ tenkh })}


                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(sdt) => this.setState({ sdt })}


                            />

                            <Text>Địa chỉ: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachi) => this.setState({ diachi })}


                            />

                            <Text>Ghi chú: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ghichu) => this.setState({ ghichu })}


                            />

                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}
