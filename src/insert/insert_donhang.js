import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_donhang extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ten_KH: '',
            id_sp: '',
            loaidon: '',
            soluong: '',
            vanchuyen: '',
            sdt: '',
            diachinhan: '',
            trangthai: '',
        }
    }

    componentDidMount1() {
        fetch('http://192.168.1.215:3000/themdonhang', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ten_KH: this.state.ten_KH,
                id_sp: parseInt(this.state.id_sp),
                loaidon: this.state.loaidon,
                soluong: parseInt(this.state.soluong),
                vanchuyen: parseInt(this.state.vanchuyen),
                sdt: this.state.sdt,
                diachinhan: this.state.diachinhan,
                trangthai: this.state.trangthai,
            }),
        });
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>

                    <ScrollView>
                        <View>
                            <Text>Tên khách hàng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ten_KH) => this.setState({ ten_KH })}


                            />

                            <Text>Sản phẩm: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(id_sp) => this.setState({ id_sp })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Loại đơn: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(loaidon) => this.setState({ loaidon })}


                            />

                            <Text>Số lượng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />

                            <Text>Vận chuyển: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(vanchuyen) => this.setState({ vanchuyen })}


                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(sdt) => this.setState({ sdt })}


                            />

                            <Text>Địa chỉ cá nhân: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachinhan) => this.setState({ diachinhan })}


                            />

                            <Text>Trạng thái: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(trangthai) => this.setState({ trangthai })}


                            />




                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}
