import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList,  Alert,ScrollView,TextInput,Button} from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_danhmuc extends Component {
  constructor(props){
    super(props);
    this.state ={ 
      isLoading: true,
       data:[],
       danhmuc: '',
       madanhmuc: ''
    }
  }

  componentDidMount1(){
    fetch('http://192.168.1.215:3000/themdanhmuc', {
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({
    tendanhmuc:this.state.danhmuc,
    madanhmuc:this.state.madanhmuc,
  }),
});
  }


  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
         
          <ScrollView>
                <View>
                    <Text>Tên danh mục: </Text>
                    <TextInput
                        placeholder="..."
                        placeholderTextColor='#85adad'
                        returnKeyType="next"
                        autoCapitalize="none"
                        autoCorrect={false}
                        onChangeText={(danhmuc) => this.setState({ danhmuc })}


                        />
                    <Text>Mã danh mục: </Text>
                    <TextInput
                        placeholder="..."
                        placeholderTextColor='#85adad'
                        // secureTextEntry
                        onChangeText={(madanhmuc) => this.setState({ madanhmuc })}
                        returnKeyType="go"
                        autoCapitalize="none"
                        autoCorrect={false}
                        />


                    <Button 
                    title="ADD"
                    color="#ff0000"
                    onPress={() => this.componentDidMount1()}>

                    </Button>


                </View>
            </ScrollView>
        </View>
      )
    }
  }
}
