import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_khachhang extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            ten: '',
            ngaysinh: "",
            gioitinh: "",
            sdt: "",
            diachi: "",
            email: "",

        }
    }

    componentDidMount1() {
        fetch('http://192.168.1.215:3000/themkhachhang', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ten: this.state.ten,
                ngaysinh: this.state.ngaysinh,
                gioitinh: this.state.gioitinh,
                sdt: this.state.sdt,
                diachi: this.state.diachi,
                email: this.state.email,
            }),
        });
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>

                    <ScrollView>
                        <View>
                            <Text>Tên khách hàng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(ten) => this.setState({ ten })}


                            />
                            <Text>Ngày sinh: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(ngaysinh) => this.setState({ ngaysinh })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Giới tính: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(gioitinh) => this.setState({ gioitinh })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Số điện thoại: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(sdt) => this.setState({ sdt })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Địa chỉ: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(diachi) => this.setState({ diachi })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Email: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(email) => this.setState({ email })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />


                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}
