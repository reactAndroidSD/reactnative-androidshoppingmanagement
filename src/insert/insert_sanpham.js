import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_sanpham extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            id_sp: '',
            tensp: '',
            anh: '',
            mavach: '',
            id_danhmuc: '',
            giavon: '',
            giaban: '',
            giabuon: '',
            soluong: '',
            mausac: '',
            trongluong: '',
            baohanh: '',
            id_nhasanxuat: '',
            mota: '',
        }
    }

    componentDidMount1() {
        fetch('http://192.168.1.215:3000/themsanpham', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_sp: parseInt(this.state.id_sp),
                tensp: this.state.tensp,
                anh: this.state.anh,
                mavach: this.state.mavach,
                id_danhmuc: parseInt(this.state.id_danhmuc),
                giavon: parseInt(this.state.giavon),
                giaban: parseInt(this.state.giaban),
                giabuon: parseInt(this.state.giabuon),
                soluong: parseInt(this.state.soluong),
                mausac: this.state.mausac,
                trongluong: parseInt(this.state.trongluong),
                baohanh: parseInt(this.state.baohanh),
                id_nhasanxuat: this.state.id_nhasanxuat,
                mota: this.state.mota,
            }),
        });
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>

                    <ScrollView>
                        <View>
                            <Text>Sản phẩm: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_sp) => this.setState({ id_sp })}


                            />
                            <Text>Tên sản phẩm: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(tensp) => this.setState({ tensp })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Ảnh: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(anh) => this.setState({ anh })}


                            />

                            <Text>Mã vạch: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mavach) => this.setState({ mavach })}


                            />

                            <Text>ID danh mục: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_danhmuc) => this.setState({ id_danhmuc })}


                            />

                            <Text>Giá vốn: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giavon) => this.setState({ giavon })}


                            />

                            <Text>Giá bán: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giaban) => this.setState({ giaban })}


                            />

                            <Text>Giá buôn: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(giabuon) => this.setState({ giabuon })}


                            />

                            <Text>Số lượng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(soluong) => this.setState({ soluong })}


                            />
                            <Text>Màu sắc: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mausac) => this.setState({ mausac })}


                            />

                            <Text>Trọng lượng: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(trongluong) => this.setState({ trongluong })}


                            />

                            <Text>Bảo hành: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(baohanh) => this.setState({ baohanh })}


                            />

                            <Text>Nhà sản xuất: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(id_nhasanxuat) => this.setState({ id_nhasanxuat })}


                            />


                            <Text>Mô tả: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(mota) => this.setState({ mota })}


                            />


                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}
