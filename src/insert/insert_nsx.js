import React, { Component } from 'react';
import { TouchableOpacity, View, ActivityIndicator, Text, FlatList, Alert, ScrollView, TextInput, Button } from 'react-native';
import nodejs from 'nodejs-mobile-react-native';
export default class insert_nsx extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: [],
            tennsx: '',
            sdt: 0,
            diachi: '',
            email: ''
        }
    }

    componentDidMount1() {
        fetch('http://192.168.1.215:3000/themnhasanxuat', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                tennsx: this.state.tennsx,
                sdt: this.state.sdt,
                diachi: this.state.diachi,
                email: this.state.email
            }),
        });
    }


    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20 }}>

                    <ScrollView>
                        <View>
                            <Text>Tên nhà sản xuất: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(tennsx) => this.setState({ tennsx })}


                            />
                            <Text>Số điện thoại NSX: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                // secureTextEntry
                                onChangeText={(sdt) => this.setState({ sdt })}
                                returnKeyType="go"
                                autoCapitalize="none"
                                autoCorrect={false}
                            />

                            <Text>Địa chỉ: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(diachi) => this.setState({ diachi })}


                            />

                            <Text>Địa chỉ email: </Text>
                            <TextInput
                                placeholder="..."
                                placeholderTextColor='#85adad'
                                returnKeyType="next"
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={(email) => this.setState({ email })}


                            />


                            <Button
                                title="ADD"
                                color="#ff0000"
                                onPress={() => this.componentDidMount1()}>

                            </Button>


                        </View>
                    </ScrollView>
                </View>
            )
        }
    }
}
