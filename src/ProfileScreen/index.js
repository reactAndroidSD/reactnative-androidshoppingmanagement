import React, { Component } from "react";
import Profile from "./Profile.js";
import EditScreenOne from "./EditScreenOne.js";
import EditScreenTwo from "./EditScreenTwo.js";
// import { StackNavigator } from "react-navigation";
import { createBottomTabNavigator } from 'react-navigation'
// export default (DrawNav = StackNavigator({
    export default (DrawNav = createBottomTabNavigator({    
  Profile: { screen: Profile },
  EditScreenOne: { screen: EditScreenOne },
  EditScreenTwo: { screen: EditScreenTwo }
}));
