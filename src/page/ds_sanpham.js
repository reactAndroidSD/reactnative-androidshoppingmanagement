  import React, { Component } from 'react';
  import { Text, View, StyleSheet, ActivityIndicator, Image, ScrollView, TouchableOpacity } from 'react-native';
  import { Header, Avatar, Icon } from 'react-native-elements';
  
  export default class ListProduct extends Component {
    static navigationOptions = {
      title: 'Danh sách sản phẩm',
    };
  
    constructor(props) {
      super(props);
      this.state ={ 
        isLoading: true,
         data:[]
      }
    }
    componentDidMount(){
      // 192.168.1.215
       fetch('http://192.168.1.215:3000/danhsachsanpham')
        .then(console.log('huhuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuhdiaudhasdhauh'))
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            data : responseJson
          }, function(){
  
          });
  
        })
        .catch((error) =>{
          console.error(error);
        });
    }
    render() {
      if(this.state.isLoading){
        return(
          <View style={{flex: 1, padding: 20}}>
            <ActivityIndicator/>
          </View>
        )
      }
      return (
        <ScrollView>
        <View>
          <View> 
          <Header
          style={{position:"absolute"}}
            leftComponent={
              <TouchableOpacity onPress={() =>
                this.props.navigation.openDrawer()}>
                <Image
                  style={styles.logo}
                  source={require('../../icons/ham.png')}
                />
              </TouchableOpacity>
  
            }
            centerComponent={{ text: 'Shopping Management', style: { color: '#fff' } }}
          />
          </View>
          
              <View style={{ backgroundColor: "rgb(215, 0, 0)", height: 70 }}>
                <Text style={{ fontSize: 30, marginBottom: 10, padding: 3, shadowRadius: 0.5, color: "white" }}>Sản phẩm</Text>
              </View>
      
              {this.state.data.map(data1 => (
                <View key = {data1.ip_sanpham}>
                  <View style={a.bao}>
                    <View style={a.trai}><Text>hinh anh</Text>
                  </View>
                  <View style={a.phai}>
                    <Text>{data1.ten_sp}</Text>
                    <Text style={{ color: "grey" }}>{data1.ma_sp}</Text>
                    <View style={{ flexDirection: "row" }}>
                      <View style={{ flex: 1 }}>
                        <Text>Giá vốn: {data1.giavon}</Text>
                        <Text>Giá bán: {data1.giaban}</Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ textAlign: "right" }}>Tồn:{data1.soluong}</Text>
                        <Text style={{ textAlign: "right" }}>Có thể bán:{data1.soluong}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            ))}
          
        </View>
        </ScrollView>
  
  
      );
    }
  }
  var a = StyleSheet.create({
    bao: {
      flexDirection: "row",
      borderBottomWidth: 1,
      marginBottom: 2,
      padding: 4
    },
    trai: {
      flex: 1,
      flexDirection: "row",
    },
    phai: {
      flex: 3
    },
  });
  
  const styles = StyleSheet.create({
  
    logo: {
        width: 50,
        height: 50,
    },
  });