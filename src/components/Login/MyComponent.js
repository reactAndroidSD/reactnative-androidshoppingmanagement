//import liraries
import React, { Component } from 'react';
import {Alert, View, Text, StyleSheet, TextInput, TouchableOpacity, KeyboardAvoidingView, StatusBar } from 'react-native';


// create a component
const MyComponent = ({navigation}) => (
    
    <TouchableOpacity 
        
    
        onPress ={() => navigation.navigate('FirstScreen')}
                
        style = {styles.buttonContainer}>
        <Text style = {styles.buttonText}>LOGIN</Text>
    </TouchableOpacity>
);

// define your styles
const styles = StyleSheet.create({
    buttonContainer: {
        backgroundColor: '#2980b9',
        paddingVertical: 15,
    },
});

//make this component available to the app
export default MyComponent;
