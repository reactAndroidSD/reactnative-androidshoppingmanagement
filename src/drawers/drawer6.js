//import liraries
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, StatusBar, TouchableOpacity, Image } from 'react-native';
import { createDrawerNavigator, createAppContainer, createStackNavigator } from 'react-navigation';
import ds_vanchuyen from '../page/ds_vanchuyen'
import insert_vanchuyen from '../insert/insert_vanchuyen'

// create a component
class MyDrawer6 extends Component {
    static navigationOptions = {
        title: 'Quản lý vận chuyển',
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

              

                <View style={styles.buttonContainer1}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('ListVC')}
                    >
                        <Text style ={styles.textStyle}>Danh Sách Vận Chuyển</Text>

                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer2}>
                    <TouchableOpacity style={{ flex: 1 }}
                        onPress={() =>
                            this.props.navigation.navigate('AddVC')}>

                        <Text style ={styles.textStyle} >Thêm Vận Chuyển</Text>
                    </TouchableOpacity>
                </View>



            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    buttonContainer1: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#00bfff',
        alignItems: 'center',
        justifyContent: 'center',

    },
    buttonContainer2: {
        flex: 1,
        // flexDirection: 'column',
        width: '100%',
        height: '100%',
        backgroundColor: '#0080ff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        color : "white",
        fontSize: 40,
        textAlignVertical: 'center'

    }
});

const AppNavigator = createStackNavigator({
    Main: {
        screen: MyDrawer6
    },
    ListVC: {
        screen: ds_vanchuyen
    },
    AddVC: {
        screen: insert_vanchuyen
    },
},
    {
        initialRouteName: "Main"
    }
);

export default createAppContainer(AppNavigator);
